#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"
source ./variables

eksctl delete cluster $K8S_CLUSTER_NAME --region $AWS_REGION
