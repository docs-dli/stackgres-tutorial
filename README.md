# StackGres tutorial

This repository contains code for running a StackGres tutorial.

This is not intended for production use. Check [StackGres Documentation](https://stackgres.io/doc/latest/03-production-installation/) for more details.

Please note that running this code in your environment may imply incur in costs from your cloud provider.

## Requirements

awscli, eksctl and kubectl should be installed following the next docs https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html
The kubectl version is a EKS-vended version, please use that.



